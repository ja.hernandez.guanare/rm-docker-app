import { Component, OnInit } from '@angular/core';
import { RmApiService } from 'src/app/shared/services/rm-api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {
  public loading = false;
  infoCount = null;
  data = [];
  dataAll = [];
  defaultImage = 'assets/images/coffee.gif';
  searchModel = '';

  pagination = {
    currentPage: 1
  };

  constructor(
    private rmApi: RmApiService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.rmApi.countCharacters().subscribe(info => {
      this.infoCount = info;
      this.getCharacters().subscribe((characters: any[]) => {
        this.dataAll = characters;
        this.loading = false;
        this.search();
      });
    });
  }

  getCharacters(page = 1): Observable<any[]> {

    const observable = new Observable<any[]>((observer) => {


      this.rmApi.getCharacters(page).subscribe((characters: any[]) => {
        observer.next(characters);
        observer.complete();
      });
    });

    return observable;
  }

  previous() {
    this.pagination.currentPage --;
    this.loading = true;
    this.getCharacters(this.pagination.currentPage).subscribe((characters: any[]) => {
      this.dataAll = characters;
      this.loading = false;
      this.search();
    });
  }


  next() {
    this.pagination.currentPage ++;
    this.loading = true;
    this.getCharacters(this.pagination.currentPage).subscribe((characters: any[]) => {
      this.dataAll = characters;
      this.loading = false;
      this.search();
    });
  }

  paginate(page) {
    this.pagination.currentPage = page;
    this.loading = true;
    this.getCharacters(this.pagination.currentPage).subscribe((characters: any[]) => {
      this.dataAll = characters;
      this.loading = false;

      this.search();
    });
  }

  search() {
    const tmpData = JSON.parse(JSON.stringify(this.dataAll));
    if (this.searchModel.length > 2) {
      this.data = tmpData.filter(character => {
        return character.name.toLowerCase().indexOf(this.searchModel.toLowerCase()) > -1 ||
                character.status.toLowerCase().indexOf(this.searchModel.toLowerCase()) > -1 ||
                character.species.toLowerCase().indexOf(this.searchModel.toLowerCase()) > -1 ||
                character.gender.toLowerCase().indexOf(this.searchModel.toLowerCase()) > -1;
      });
    } else {
      this.data = tmpData;
    }

  }

  // tslint:disable-next-line: variable-name
  fakeArray(number: number) {
    const arr = [];
    for (let index = 0; index < number; index++) {
      arr.push(index + 1 );
    }
    return arr;
  }

}
