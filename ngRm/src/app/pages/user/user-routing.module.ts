import { AuthGuard } from './../../shared/helpers/auth.guard';
import { CharacterComponent } from './character/character.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'character',
     component: CharacterComponent,
     canActivate: [AuthGuard],
    data: {
      title: 'Characters'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
