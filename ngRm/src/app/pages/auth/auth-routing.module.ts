import { LogoutComponent } from './logout/logout.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './../../shared/helpers/auth.guard';
import { AuthComponent } from './auth.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
     component: AuthComponent,
     canActivate: [AuthGuard],
    data: {
      title: 'Auth page'
    },
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  /*{
    path: 'register',
    component: RegistroComponent,
    data: {
      title: 'Registro'
    }
  },*/
  {
    path: 'logout',
    component:  LogoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Logout'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
