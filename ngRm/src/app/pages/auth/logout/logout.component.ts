import { Component, OnInit } from '@angular/core';
import { RmApiService } from 'src/app/shared/services/rm-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(
    private rmApi: RmApiService,
    private router: Router
  ) { }

  ngOnInit() {
    console.log('pasa por aqui...');
    this.rmApi.logoutLocal();
    this.router.navigate(['auth/login'], { replaceUrl: true });
    /*this.rmApi.logout().subscribe(resp => {
      console.log('logout 1', localStorage.getItem('currentUser'));
      this.router.navigate(['auth/login'], { replaceUrl: true });

    }, error => {
      console.log(error);
      console.log('logout 2');
      this.router.navigate(['auth/login'], { replaceUrl: true });
      // localStorage.removeItem('currentUser');
    });*/


  }

}
