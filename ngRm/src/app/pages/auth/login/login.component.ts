import { RmApiService } from './../../../shared/services/rm-api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild('formLogin', { static: false }) formLogin: NgForm;
  @ViewChild('formRegister', { static: false }) formRegister: NgForm;

  loginOrRegister = 'login';
  public loading = false;

  form = {
    user: '',
    password: '',
    repassword: '',
    errorLogin: '',
    errorRegister: ''
  };

  formBase = {
    user: '',
    password: '',
    repassword: '',
    errorLogin: '',
    errorRegister: ''
  };

  constructor(
    private rmAPi: RmApiService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  changeForm(action) {
    this.loginOrRegister = action;
    // this.form = JSON.parse(JSON.stringify(this.formBase));
  }

  login() {

    // this.formLogin.form.markAllAsTouched();
    this.loading = true;
    this.rmAPi.login(this.form.user, this.form.password).subscribe((userLogged: any) => {
      this.loading = false;
      this.router.navigate(['/auth'], { replaceUrl: true });
      console.log(userLogged);
      this.rmAPi.launchToastr('Login successfully', 'you have logged successfully', 'success');

    }, error => {
      this.form.errorLogin = error;
      console.log(error);
      this.loading = false;
      this.rmAPi.launchToastr('Failed', error, 'error');
    });

  }

  register() {

    // this.formLogin.form.markAllAsTouched();
    if (this.form.password.length === this.form.repassword.length) {
      this.loading = true;
      this.rmAPi.register(this.form.user, this.form.password).subscribe((userRegistered: any) => {

        this.rmAPi.login(this.form.user, this.form.password).subscribe((userLogged: any) => {
          this.loading = false;
          this.router.navigate(['/auth'], { replaceUrl: true });
          console.log(userLogged);
          this.rmAPi.launchToastr('User registered ', 'you has registered successfully', 'success');

        }, error => {
          this.form.errorLogin = error;
          console.log(error);
          this.loading = false;
          this.rmAPi.launchToastr('Failed', error, 'error');
        });

      }, error => {
        this.form.errorRegister = error;
        console.log(error);
        this.loading = false;
        this.rmAPi.launchToastr('Failed', error, 'error');
      });

    } else {
      const error = 'Passwords do not match.';
      this.form.errorRegister = error;
      this.rmAPi.launchToastr('Failed', error, 'error');
    }


  }
}
