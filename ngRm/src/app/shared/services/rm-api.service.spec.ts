import { TestBed } from '@angular/core/testing';

import { RmApiService } from './rm-api.service';

describe('RmApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RmApiService = TestBed.get(RmApiService);
    expect(service).toBeTruthy();
  });
});
