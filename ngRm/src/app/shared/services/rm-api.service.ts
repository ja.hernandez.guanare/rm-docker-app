import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RmApiService {
  urlServer = 'http://localhost:3000';
  constructor(
    private http: HttpClient,
    public toastr: ToastrService,
    ) { }


  launchToastr(title, msg, type) {
    switch (type) {
      case 'success':
        this.toastr.success(msg, title);
        break;
      case 'error':
        this.toastr.error(msg, title);
        break;
      case 'warning':
        this.toastr.warning(msg, title);
        break;
      case 'info':
        this.toastr.info(msg, title);
        break;
      default:
        break;
    }
  }

  login(user: string, password: string) {
    return this.http.post(this.urlServer + '/login',

      // tslint:disable-next-line: object-literal-shorthand
      { user: user, password: password }).pipe(
        map((userLogin: any) => {
          // login successful if there's a jwt token in the response
          if (userLogin && userLogin.logged.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(userLogin.logged));
          }
          return userLogin.logged;
        })
      );
  }

  logoutLocal() {
    localStorage.removeItem('currentUser');
  }

  logout(): Observable<object> {

    const observable = new Observable<object>((observer) => {

      // localStorage.removeItem('currentUser');
      // remove user from local storage to log user out
      const loggedUser = this.testJSON(localStorage.getItem('currentUser')) ?  JSON.parse(localStorage.getItem('currentUser')) : null;
      if (loggedUser) {
         this.http.post(this.urlServer + '/logout', { }).subscribe( resp => {
          localStorage.removeItem('currentUser');
          observer.next(resp);
          observer.complete();
        });
      } else {
        localStorage.removeItem('currentUser');
        observer.next({ message: 'user logout' });
        observer.complete();
      }
    });

    return observable;
  }

  register(user: string, password: string) {
    return this.http.post(this.urlServer + '/register',
    // tslint:disable-next-line: object-literal-shorthand
    { user: user, password: password });
  }

  countCharacters() {
    return this.http.get(this.urlServer + '/rm-character-count');
  }

  getCharacters(page = 1) {

    const pageParam = page > 1 ? '/' + page : '';
    return this.http.get(this.urlServer + '/rm-character' + pageParam);
  }

  getCurrentUser() {
   return this.testJSON(localStorage.getItem('currentUser')) ?  JSON.parse(localStorage.getItem('currentUser')) : null;
  }

  testJSON(valor) {
    try {
      return JSON.parse(valor);
    } catch (error) {
      return null;
    }
  }

}
