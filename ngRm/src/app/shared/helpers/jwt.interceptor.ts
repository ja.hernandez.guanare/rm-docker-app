import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RmApiService } from '../services/rm-api.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private rmApi: RmApiService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const currentUser = this.rmApi.getCurrentUser();
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    'access-token': `${currentUser.token}`
                }
            });
        }

        return next.handle(request);
    }
}
