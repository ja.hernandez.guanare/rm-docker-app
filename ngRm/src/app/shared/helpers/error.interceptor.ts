import { RmApiService } from './../services/rm-api.service';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private rmApi: RmApiService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
              console.log('No auth');
                // auto logout if 401 response returned from api
              this.rmApi.logoutLocal();
              location.reload();
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }
}
