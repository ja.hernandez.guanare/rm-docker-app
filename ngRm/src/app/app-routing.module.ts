import { BlankComponent } from './layouts/blank/blank.component';
import { FullComponent } from './layouts/full/full.component';
import { Routes } from '@angular/router';


export const Approutes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: 'users',
        loadChildren: './pages/user/user.module#UserModule'
      }
    ]
  },
  {
    path: '',
    component: BlankComponent,
    children: [
      {
        path: 'auth',
        loadChildren:
          './pages/auth/auth.module#AuthModule'
      }
    ]
  },
  { path: '**', redirectTo: '' }
];
