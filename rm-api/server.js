const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var cors = require('cors');

app.use(cors());
console.log('with cors!!!');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 

const rmRoutes = require('./routes/rm-routes');
app.use('/', rmRoutes);

/*app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});*/





const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});