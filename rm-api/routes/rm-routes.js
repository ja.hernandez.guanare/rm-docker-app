
const express = require('express');
const router = express.Router();
const path = require('path');
const moment = require('moment');
const redisClient = require(path.resolve(__dirname, '../redis-client'));
const jwt =  require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt_secret = "jwtfanhere";

const rp = require('request-promise');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.json({ "message": "Hello to Rick and Morty API v1.0.0..." });
});


router.get('/rm-character-count', async (req, res, next) => {
console.log('req.headers: ', req.headers);

    const token = req.headers['access-token'];
    //  const token = req.headers['access-token'];
    const valid_token = await validateToken(token);
    if(!valid_token) {
        return res.status(401).json({
            status: 'error',
            message: 'Authorization Required'
        });
    } else {
        let url = 'http://rickandmortyapi.com/api/character/';
        console.log('req.body.token: ', req.body.token);     
        const resp = await rp(url);
        json_resp = JSON.parse(resp);   
        res.json(json_resp.info);
    }
    
  
});

router.get('/rm-character/:page?*', async (req, res, next) => {
   
    const token = req.headers['access-token'];
    const valid_token = await validateToken(token);
    if(!valid_token) {
        return res.status(401).json({
            status: 'error',
            message: 'Authorization Required'
        });
    } else {
        let url = 'http://rickandmortyapi.com/api/character/';
        const page = req.params.page ? parseInt(req.params.page) : 0;
        if(isNaN(page)) {
            return res.status(400).json({
                status: 'error',
                message: `Pagination not a number.`
            });
        } else {
            url += '?page=' + page;
            const resp = await rp(url);
            json_resp = JSON.parse(resp);
            const result_filter = json_resp.results.map(character => {
                return {
                    name: character.name,
                    status: character.status,
                    species: character.species,
                    gender: character.gender,
                    image: character.image
                }
            });
            // console.log(result_filter);
            res.json(result_filter);
        }
    }
  
});




router.post("/register", async (req, res, next) => {



    let error = validateUserPasswordParams(req, res);


    const user_key = 'user:' + req.body.user;
    const user_exists = await redisClient.getAsync(user_key);
    console.log(user_exists);

    if (user_exists) {
        error = res.status(400).json({
            status: 'error',
            message: `The ${req.body.user} already exists.`
        });
    }

    if (error) {
        return error;
    } else {

        const salt = await bcrypt.genSalt(saltRounds);
        const hash = await bcrypt.hash(req.body.password, salt);


        await redisClient.setAsync(user_key, hash);

        return res.status(200).json({
            status: 'ok',
            message: `user ${req.body.user} created.`
        });
    }

});

router.post("/login", async (req, res, next) => {

    // When user logs in, there is no token pair in the browser
    // cookies. We need to issue both of them. Because you also
    // log user in in this step, I assume that you already have


    let error = validateUserPasswordParams(req, res);

    const user_key = 'user:' + req.body.user;
    const user_hash = await redisClient.getAsync(user_key);
    console.log(user_hash);

    if (user_hash === null) {
        error = res.status(400).json({
            status: 'error',
            message: `Incorrect username or password.`
        });
    } else {
        const pass_compare = await bcrypt.compare(req.body.password, user_hash);

        if(!pass_compare) {
            error = res.status(400).json({
                status: 'error',
                message: `Incorrect username or password.`
            });
        }
    }

    if (error) {
        return error;
    } else {
        
        // Generate new access token
        let token = jwt.sign({ uid: req.body.user }, jwt_secret, {expiresIn: '120s'});
        
        // And store the user in Redis under key 2212
        const user_key = 'token:' + req.body.user;
        const logged = {
            user: req.body.user,
            token: token,
            ttl: moment.utc().add(120, 'seconds').toDate()
        };
        await redisClient.setAsync(user_key, JSON.stringify(logged));

        return res.status(200).json({
            status: 'ok',
            message: `user ${req.body.user} logged.`,
            logged: logged
        });
    }


});

router.post("/logout", async (req, res, next) => {

    const token = req.headers['access-token'];

    const valid_token = await validateToken(token);
    if(!valid_token) {
        return res.status(401).json({
            status: 'error',
            message: 'Authorization Required'
        });
    } else {
        console.log('valid_token: ', valid_token);

        token = jwt.sign({ uid: valid_token }, jwt_secret, {expiresIn: '1s'});

        const user_key = 'token:' + valid_token;
        const logged = {
            user: valid_token,
            token: token,
            ttl: moment.utc().add(1, 'second').toDate()
        };
        await redisClient.setAsync(user_key, JSON.stringify(logged));

        return  res.json({ "message": "user logout" });
    }


});

 validateToken = async (token) => {
 
   return  new Promise((resolve, reject) => {
       if (token === undefined) {
        resolve(null);
       } else {
        jwt.verify(token, jwt_secret, (error, payload) => {
            if (error) {
              resolve(null);
            } else {
              const user_key = 'token:' + payload.uid;
              redisClient.getAsync(user_key).then(user_exists => {
      
                  if (user_exists) {
                      const logged = JSON.parse(user_exists);
                      resolve(logged.token === token ? payload.uid : null);
                  } else {
                      resolve(null);
                  }
                 
              });
          
            }
          });
       }
   
  });

    
}

validateUserPasswordParams = (req, res) => {

    const require_params = ['user', 'password'];
    let error = null;
    require_params.forEach(param => {

        if (req.body[param] === undefined) {
            error = res.status(400).json({
                status: 'error',
                message: `Missing required argument ${param}.`,
            })
        } else if (req.body[param].length === 0) {
            error = res.status(400).json({
                status: 'error',
                message: `The ${param} field is required.`
            });
        }

    });
    return error;
}


module.exports = router;

